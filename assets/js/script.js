function navbarToggle() {
	var x = document.getElementById("menu");
	if (x.style.display === "block") {
    	x.style.display = "none";
	} 	
	else {
		x.style.display = "block";
	}
}

var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
	var currentScrollPos = window.pageYOffset;
  	if (prevScrollpos > currentScrollPos || currentScrollPos < 35) {
    	document.getElementById("navbar").style.top = "0";
  	} else {
    	document.getElementById("navbar").style.top = "-120px";
  	}
  prevScrollpos = currentScrollPos;
}